import { Button, Form } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';

export default function CartTable({productProp}) {
	console.log(productProp)

	const {productName, productId, price, _id, inStock, quantity} = productProp;

	let [count, setCount] = useState(quantity);

	function incrementCount() {
	    count = count + 1;
	    setCount(count);
  }
  function decrementCount() {
      count = count - 1;
      setCount(count);
  }

  let subtotal = (price * count)

  const deleteOrder = (e, productId) => {
  	e.preventDefault();
  	fetch(`https://tranquil-sea-79777.herokuapp.com/users/order`, {
  		method: 'PUT',
  		headers: {
  			Authorization: `Bearer ${localStorage.getItem('token')}`
  		},
      body: JSON.stringify({
        productId: productId
      })
  	})
  	.then(res => res.json())
  	.then(data => {
  		console.log(data);

  		if(data){
  			Swal.fire({
  				title: 'Item Removed',
  				icon: 'success',
  				text: `${productName} is no longer in your cart`
  			})

  		} else {
  			Swal.fire({
  				title: 'Something went wrong',
  				icon: 'error',
  				text: 'Please try again later'
  			})
  		}
  	})
  }

	return (
    	<>
        <tr className="styleProdTab">
          <td>{productName}</td>
          <td className="alignCenter">${price}</td>
          <td>
    		    <div className="quantity">
    		    	<button onClick={decrementCount}>-</button>
    		      <div>{count}</div>
    		      <button onClick={incrementCount}>+</button>
    	      </div>
          </td>
          <td className="alignCenter">${subtotal}</td>
          <td className="alignCenter">
          	<Button className="btn-danger" onClick={(e) => deleteOrder(e, productId)}>Delete</Button>
          </td>
          	
        </tr>
      </>
    )
}
